package com.citi.test;

import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.assertThat;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import com.citi.trading.Investor;
import com.citi.trading.Market;

import java.util.HashMap;
import com.citi.trading.OrderPlacer;
public class InvestorUnitTest {
	
	Investor iv;
	
	@Before
	public void setUp() {
		Map portfolio = new HashMap<String, Integer>();
		portfolio.put("Apple", 100);
		iv = new Investor(portfolio, 10000);
	}
	
	@After
	public void tearDown() {
		iv = null;
	}
	@Test
    public void testBuy()
    {
		OrderPlacer m = new MockMarket();
		iv.setMarket(m);
		
		iv.buy("Apple", 100, 100);
		assertThat(iv.getCash(),closeTo(0,0.01));
		iv.sell("Apple", 100, 900);
		assertThat(iv.getCash(),closeTo(90000,0.01));

    }
	
	@Test
    public void testSell() {
		OrderPlacer m = new MockMarket();
		iv.setMarket(m);
		iv.sell("Apple", 100, 900);
		assertThat(iv.getCash(),closeTo(100000,0.01));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void invalidPortfolio() {
		Map portfolio = new HashMap<String, Integer>();
		portfolio.put("Apple", -100);
		Investor invalid = new Investor(portfolio, 100);
	}
}
