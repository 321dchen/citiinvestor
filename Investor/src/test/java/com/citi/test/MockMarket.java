package com.citi.test;

import java.util.function.Consumer;

import javax.jms.Message;
import com.citi.trading.Trade;
import com.citi.trading.Market;
import com.citi.trading.OrderPlacer;

public class MockMarket implements OrderPlacer {

	public MockMarket() {
	}

	public synchronized void placeOrder(Trade order, Consumer<Trade> callback) {
		callback.accept(order);
	}

	public synchronized void onMessage(Message message) {

	}
}
